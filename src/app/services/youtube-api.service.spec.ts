import { TestBed } from '@angular/core/testing';
import { videos } from './customVideos';
import { HttpTestingController } from '@angular/common/http/testing';
import { YoutubeApiService } from './youtube-api.service';
import { environment } from 'src/environments/environment';

describe('YoutubeApiService', () => {
  let service: YoutubeApiService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    httpTestingController = TestBed.get(HttpTestingController)
    service = TestBed.inject(YoutubeApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('#getData should return expected data', (done) => {
    const expectedData = videos;

    service.getYoutubeData().subscribe(data => {
      expect(data).toEqual(expectedData);
      done();
    });

    const testRequest = httpTestingController.expectOne('https://www.googleapis.com/youtube/v3/search?key=' + environment.keyYoutube + '&maxResults=50&type=video&part=snippet&q=john');

    testRequest.flush(expectedData);
  });

  it('test endpoint method', () => {
    const expectedData = videos;
    service.getYoutubeData().subscribe();
    const testRequest = httpTestingController.expectOne('https://www.googleapis.com/youtube/v3/search?key=' + environment.keyYoutube + '&maxResults=50&type=video&part=snippet&q=john');
    expect(testRequest.request.method).toEqual('GET');
  });
  it('test return for error', () => {
    const expectedData: jasmine.Expected<Object> = [];
    service.getYoutubeData().subscribe(data => {
      expect(data).toEqual(expectedData);
      done()
    });
    const testRequest = httpTestingController.expectOne('https://www.googleapis.com/youtube/v3/search?key=' + environment.keyYoutube + '&maxResults=50&type=video&part=snippet&q=john');
    testRequest.flush('error', { status: 500, statusText: 'Broken Service' })
  });

});
function done() {
  throw new Error('Function not implemented.');
}

