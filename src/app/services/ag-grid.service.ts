import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AgGridComponent } from '../ag-grid/ag-grid.component';

@Injectable({
  providedIn: 'root'
})
export class AgGridService {
  private agGridComponent: AgGridComponent | undefined;
  private seletctedVideoListe = new BehaviorSubject<number>(0);

  constructor(
  ) { }
  currentListeSize = this.seletctedVideoListe.asObservable();
  getAgGridComponent(): (AgGridComponent | undefined) {
    return this.agGridComponent;
  }

  setAgGridComponent(agGridComponent: AgGridComponent) {
    this.agGridComponent = agGridComponent;
  }
  changeListeSize(newSize: number) {
    this.seletctedVideoListe.next(newSize)
  }

}
