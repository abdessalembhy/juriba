import { videos } from './customVideos';
import { SearchPipe } from './search.pipe';

describe('SearchPipe', () => {
  it('create an instance', () => {
    const pipe = new SearchPipe();
    const search= 'dsds'
    const res = pipe.transform(videos,search)
    expect(res.length).toBeLessThan(videos.length);
  });
});
