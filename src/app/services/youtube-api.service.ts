import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class YoutubeApiService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private http: HttpClient) { }

  getYoutubeData() {
    return this.http.get('https://www.googleapis.com/youtube/v3/search?key=' + environment.keyYoutube + '&maxResults=50&type=video&part=snippet&q=john',
      this.httpOptions);
  }


}
