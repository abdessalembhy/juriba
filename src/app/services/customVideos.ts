export const videos =  [
    {
        "kind": "youtube#searchResult",
        "etag": "uALS86lCAzchQZt8ki1Fd0tNbKE",
        "id": {
            "kind": "youtube#video",
            "videoId": "ChiLWi-mBvU"
        },
        "snippet": {
            "publishedAt": "2020-11-22T18:00:02Z",
            "channelId": "UCJ5v_MCY6GNUBTO8-D3XoAg",
            "title": "FULL MATCH - John Cena &amp; The Rock vs. The Miz &amp; R-Truth: Survivor Series 2011",
            "description": "The Rock and John Cena form a dream team to take on The Miz & R-Truth at Survivor Series 2011: Courtesy of WWE Network. WWE Network | Subscribe now: ...",
            "thumbnails": {
                "default": {
                    "url": "https://i.ytimg.com/vi/ChiLWi-mBvU/default.jpg",
                    "width": 120,
                    "height": 90
                },
                "medium": {
                    "url": "https://i.ytimg.com/vi/ChiLWi-mBvU/mqdefault.jpg",
                    "width": 320,
                    "height": 180
                },
                "high": {
                    "url": "https://i.ytimg.com/vi/ChiLWi-mBvU/hqdefault.jpg",
                    "width": 480,
                    "height": 360
                }
            },
            "channelTitle": "WWE",
            "liveBroadcastContent": "none",
            "publishTime": "2020-11-22T18:00:02Z"
        }
    },
    {
        "kind": "youtube#searchResult",
        "etag": "Rz1rZfXLQbDEJp8sNLLzadlzdRQ",
        "id": {
            "kind": "youtube#video",
            "videoId": "ZZT-fj9bsQE"
        },
        "snippet": {
            "publishedAt": "2021-02-06T05:00:12Z",
            "channelId": "UCeSb0pP7FfQYE7wFFIQW-VQ",
            "title": "Serie de Among Us Temporada 1 Completa: Cr0no, Ion y D4rk",
            "description": "Recuerda dejar tu like en esta primera temporada de la serie ficticia de john falz, es decir la temporada 1 de among us completa donde los impostores Cr0no, ...",
            "thumbnails": {
                "default": {
                    "url": "https://i.ytimg.com/vi/ZZT-fj9bsQE/default.jpg",
                    "width": 120,
                    "height": 90
                },
                "medium": {
                    "url": "https://i.ytimg.com/vi/ZZT-fj9bsQE/mqdefault.jpg",
                    "width": 320,
                    "height": 180
                },
                "high": {
                    "url": "https://i.ytimg.com/vi/ZZT-fj9bsQE/hqdefault.jpg",
                    "width": 480,
                    "height": 360
                }
            },
            "channelTitle": "John Falz",
            "liveBroadcastContent": "none",
            "publishTime": "2021-02-06T05:00:12Z"
        }
    },
    {
        "kind": "youtube#searchResult",
        "etag": "_ubS8Pop7rHznyIYPwFFzGW1Dpc",
        "id": {
            "kind": "youtube#video",
            "videoId": "TtkDqZRlcnM"
        },
        "snippet": {
            "publishedAt": "2021-08-20T11:52:01Z",
            "channelId": "UCZ4w9wYsmb0gC7ZQYNMdKAA",
            "title": "CHEGA E SENTA - JOHN AMPLIFICADO E HENRIQUE (H&amp;J) (LANÇAMENTO 2021)",
            "description": "Vídeo não oficial sem intenção de violar os termos de direitos autorais, sendo assim se sentir os direitos violados não nos envie strike, nos envie um e-mail que ...",
            "thumbnails": {
                "default": {
                    "url": "https://i.ytimg.com/vi/TtkDqZRlcnM/default.jpg",
                    "width": 120,
                    "height": 90
                },
                "medium": {
                    "url": "https://i.ytimg.com/vi/TtkDqZRlcnM/mqdefault.jpg",
                    "width": 320,
                    "height": 180
                },
                "high": {
                    "url": "https://i.ytimg.com/vi/TtkDqZRlcnM/hqdefault.jpg",
                    "width": 480,
                    "height": 360
                }
            },
            "channelTitle": "OS INIGUALÁVEIS",
            "liveBroadcastContent": "none",
            "publishTime": "2021-08-20T11:52:01Z"
        }
    },
    {
        "kind": "youtube#searchResult",
        "etag": "77RhcnJ8GXErE-KpNlXl4zqIToQ",
        "id": {
            "kind": "youtube#video",
            "videoId": "3g7TZLaBknc"
        },
        "snippet": {
            "publishedAt": "2020-11-04T06:00:09Z",
            "channelId": "UCc77CW_DkpIjbyVInxQP1Kw",
            "title": "Beki May Nahanap ng Jowa sa Omegle? | Omegle Prank 7.2 | John Fedellaga",
            "description": "Hi mga Beks!! (WATCH IN HD!) For today's video mag hahanap unit tayo ng Jowa sa Omegle!! Beks! Make sure to subscribe to my channel and click the bell ...",
            "thumbnails": {
                "default": {
                    "url": "https://i.ytimg.com/vi/3g7TZLaBknc/default.jpg",
                    "width": 120,
                    "height": 90
                },
                "medium": {
                    "url": "https://i.ytimg.com/vi/3g7TZLaBknc/mqdefault.jpg",
                    "width": 320,
                    "height": 180
                },
                "high": {
                    "url": "https://i.ytimg.com/vi/3g7TZLaBknc/hqdefault.jpg",
                    "width": 480,
                    "height": 360
                }
            },
            "channelTitle": "John Fedellaga",
            "liveBroadcastContent": "none",
            "publishTime": "2020-11-04T06:00:09Z"
        }
    },
    {
        "kind": "youtube#searchResult",
        "etag": "xJ0hALSHDDs5L0qP7VuEDQqttLQ",
        "id": {
            "kind": "youtube#video",
            "videoId": "NvpKES_kcYg"
        },
        "snippet": {
            "publishedAt": "2021-08-26T00:03:57Z",
            "channelId": "UC3XTzVzaHQEd30rQbuvCtTQ",
            "title": "Last Week Tonight&#39;s Masterpiece Gallery Tour",
            "description": "You are cordially invited to attend Last Week Tonight's Masterpiece Gallery Tour. Visit lwtgallery.com or johnoliverhasyourraterotica.com for details. Connect with ...",
            "thumbnails": {
                "default": {
                    "url": "https://i.ytimg.com/vi/NvpKES_kcYg/default.jpg",
                    "width": 120,
                    "height": 90
                },
                "medium": {
                    "url": "https://i.ytimg.com/vi/NvpKES_kcYg/mqdefault.jpg",
                    "width": 320,
                    "height": 180
                },
                "high": {
                    "url": "https://i.ytimg.com/vi/NvpKES_kcYg/hqdefault.jpg",
                    "width": 480,
                    "height": 360
                }
            },
            "channelTitle": "LastWeekTonight",
            "liveBroadcastContent": "none",
            "publishTime": "2021-08-26T00:03:57Z"
        }
    }
];