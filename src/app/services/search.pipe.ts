import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(videos: any, search: string): any {
    if (!videos) {
      return null;
    }
    if (!search) {
      return videos;
    }
    return videos.filter((video: any) => {
      return video.snippet.title.includes(search);
    });
  }
}
