import { ComponentFixture, TestBed } from '@angular/core/testing';
import { videos } from '../services/customVideos';

import { AgGridComponent } from './ag-grid.component';

describe('AgGridComponent', () => {
  let component: AgGridComponent;
  let fixture: ComponentFixture<AgGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AgGridComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('test imageCellRenderer function', () => {
    expect(component.imageCellRenderer(videos[0])).toBe(`<img alt="FULL MATCH - John Cena &amp; The Rock vs. The Miz &amp; R-Truth: Survivor Series 2011" src="https://i.ytimg.com/vi/ChiLWi-mBvU/default.jpg">`);
  });
  it('test titleRender function', () => {
    expect(component.titleRender(videos[0])).toBe(`<a href=https://www.youtube.com/watch?v='ChiLWi-mBvU>FULL MATCH - John Cena &amp; The Rock vs. The Miz &amp; R-Truth: Survivor Series 2011</a>`);
  });
});
