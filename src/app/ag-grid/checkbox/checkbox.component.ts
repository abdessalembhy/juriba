import { Component, OnDestroy, Self } from '@angular/core';
import { Subscription } from 'rxjs';
import { AgGridService } from 'src/app/services/ag-grid.service';
import { YoutubeApiService } from 'src/app/services/youtube-api.service';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
})
export class CheckboxComponent implements OnDestroy {
  public params: any;
  videoListeSize = 0;
  selectedVideoSubscription: Subscription | undefined;

  constructor(private agGridService: AgGridService) { }


  agInit(params: any): void {
    this.params = params;
    this.selectedVideoSubscription = this.agGridService.currentListeSize.subscribe(size => this.videoListeSize = size)

  }

  checkedHandler(event: any) {
    const checked = event.target.checked;
    const colId = this.params.column.colId;
    this.params.node.setDataValue(colId, checked);
    if (checked) {
      this.agGridService.changeListeSize(this.videoListeSize += 1);
    } else {
      this.agGridService.changeListeSize(this.videoListeSize -= 1);
    }
  }
  ngOnDestroy() {
    this.selectedVideoSubscription?.unsubscribe()
  }
}
