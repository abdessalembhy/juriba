import { Component, OnDestroy, OnInit, Self } from '@angular/core';
import { Subscription } from 'rxjs';
import { YoutubeApiService } from '../services/youtube-api.service';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { CustomHeaderComponent } from './custom-header/custom-header.component';
import { AgGridService } from '../services/ag-grid.service';
import * as utilFunctions from './cellRender';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-ag-grid',
  templateUrl: './ag-grid.component.html',
  styleUrls: ['./ag-grid.component.scss'],
  providers: [YoutubeApiService]
})
export class AgGridComponent implements OnInit, OnDestroy {

  public rowData =[];
  public totalVideo = 0;
  public videoList: any;
  private youtubeSubscription: Subscription | undefined;
  private selectedVideoSubscription: Subscription | undefined;
  public youtubeEndpointResult: any;
  public showSelection = false;
  public videoSelectedListeSize = 0;
  public columnDefs: any;
  private columnDefswithoutCheck = [
    {
      headerName: '', cellRenderer: this.imageCellRenderer, minWidth: 70,
      width: 70,
      maxWidth: 70,
      sortable: true
    },
    { type: 'link', headerName: 'Title', cellRenderer: this.titleRender , sortable: true },
    { headerName: 'Description', field: 'snippet.description', sortable: true },
    { headerName: 'Published on', field: 'snippet.publishedAt', type: ['dateColumn', 'nonEditableColumn'], sortable: true }
  ];
  public columnDefswithCheck = [
    { headerName: '', cellRenderer: "CheckboxComponent", field: 'checked', maxWidth: 100, isHeaderCheckbox: true },
    {
      headerName: '', cellRenderer: this.imageCellRenderer, minWidth: 70,
      width: 70,
      maxWidth: 70,
      sortable: true
    },
    { type: 'link', headerName: 'Title', cellRenderer: this.titleRender, sortable: true },
    { headerName: 'Description', field: 'snippet.description', sortable: true },
    { headerName: 'Published on', field: 'snippet.publishedAt', type: ['dateColumn', 'nonEditableColumn'],sortable: true }
  ];
  frameworkComponents = {
    CheckboxComponent: CheckboxComponent,
    agColumnHeader: CustomHeaderComponent
  };

  constructor(
    @Self() private youtubeService: YoutubeApiService,
    public snackBar: MatSnackBar,
    private agGridService: AgGridService) { }

  ngOnInit(): void {
    this.agGridService.setAgGridComponent(this);
    this.selectedVideoSubscription = this.agGridService.currentListeSize.subscribe(size => this.videoSelectedListeSize = size)
    this.columnDefs = this.columnDefswithoutCheck;
    this.youtubeSubscription = this.youtubeService.getYoutubeData().subscribe(result => {
    this.youtubeEndpointResult = result;
    this.videoList = this.youtubeEndpointResult.items
    this.videoList = this.videoList.map((video: any) => ({ ...video, checked: false }));
    this.rowData = this.videoList;
    this.totalVideo = this.rowData.length;
    },
    (error) => {
      this.openSnackBar('server error (check the api key)', 'error', 'alert-red');
    })
  }

  imageCellRenderer(params: any) {
    return utilFunctions.imagecellRenderer(params);  }

  titleRender(params:any) {
    return utilFunctions.titleRender(params);
  }

  // show-hide video selection
  toggle() {
    this.showSelection = !this.showSelection;
    this.showSelection ? this.columnDefs = this.columnDefswithCheck : this.columnDefs = this.columnDefswithoutCheck
  }



  // checkAll video method
  checkall() {
    if (this.videoSelectedListeSize !== this.videoList.length) {
      this.videoList = this.videoList.map((video: any) => ({ ...video, checked: true }))
      this.rowData = this.videoList;

      this.agGridService.changeListeSize(this.videoList.length);
    } else {
      this.videoList =this.videoList.map((video: any) => ({ ...video, checked: false }))
      this.rowData = this.videoList;

      this.agGridService.changeListeSize(0);
    }
  }
  // custom context menu ag-grid
  getContextMenuItems(params: any) {
    if (params.column.colDef.headerName === 'Title') {
      var result = [
        {
          name: `${params.node.data.snippet.title}`,
          action: function () {
            window.open(`https://www.youtube.com/watch?v='${params.node.data.id.videoId}`, '_blank')
          },
        },];
    } else {
      result = [];
    }
    return result;
  }

  // recive event from search component
  receiveListAfterSearch($event: []) {
    this.rowData = $event;
  }
  openSnackBar(message: string, action: string, className: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      horizontalPosition: 'center',
      panelClass: [className],
    });
  }
  ngOnDestroy() {
    this.youtubeSubscription?.unsubscribe();
    this.selectedVideoSubscription?.unsubscribe()
  }
}
