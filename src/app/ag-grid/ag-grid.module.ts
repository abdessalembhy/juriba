import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgGridComponent } from './ag-grid.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { GridRoutingModule } from './grid-routing.module';
import { AgGridModule } from 'ag-grid-angular';
import'ag-grid-enterprise';
import { SearchComponentComponent } from './search-component/search-component.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatInputModule } from '@angular/material/input';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchPipe } from '../services/search.pipe';
import { MatButtonModule } from '@angular/material/button';
import { CustomHeaderComponent } from './custom-header/custom-header.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';



@NgModule({
  declarations: [
    AgGridComponent,
    CheckboxComponent,
    SearchComponentComponent,
    CustomHeaderComponent,
    
  ],
  imports: [
    CommonModule,
    GridRoutingModule,
    MatCardModule,
    MatCheckboxModule,
    MatInputModule,
    MatGridListModule,
    ReactiveFormsModule,
    FormsModule,
    MatSnackBarModule,
    MatInputModule,
    MatSlideToggleModule,
    MatButtonModule,
    AgGridModule.withComponents([CheckboxComponent, CustomHeaderComponent]),
  ],
  providers:[SearchPipe]
})
export class GridModule { }
