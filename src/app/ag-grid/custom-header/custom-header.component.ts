import { Component } from '@angular/core';
import { IHeaderAngularComp } from 'ag-grid-angular';
import { IHeaderParams } from 'ag-grid-community';
import { AgGridService } from 'src/app/services/ag-grid.service';

@Component({
  selector: 'app-custom-header',
  templateUrl: './custom-header.component.html',
  styleUrls: ['./custom-header.component.scss']
})
export class CustomHeaderComponent implements IHeaderAngularComp {
  params: any;
  agGridComponent: any;
  constructor(private agGridService: AgGridService) { }
  refresh(params: IHeaderParams): boolean {
    return false;
  }
  agInit(params: IHeaderParams): void {
    this.agGridComponent = this.agGridService.getAgGridComponent();
    this.params = params;
  }

  selectAllCheck() {
    if (this.agGridComponent !== undefined) {
      this.agGridComponent.checkall();
    }
  }
}
