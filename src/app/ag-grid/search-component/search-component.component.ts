import { Component, Input, OnInit, Output, EventEmitter, Self } from '@angular/core';
import { AgGridService } from 'src/app/services/ag-grid.service';
import { SearchPipe } from 'src/app/services/search.pipe';

@Component({
  selector: 'app-search-component',
  templateUrl: './search-component.component.html',
  styleUrls: ['./search-component.component.scss']
})
export class SearchComponentComponent {
  public currentValue = '';
  @Input() videoList: [] | undefined;
  newVideoList: [] | undefined;
  @Output() newListEvent = new EventEmitter<[]>();
  constructor(private searchPipe: SearchPipe, private agGridService : AgGridService) { }

  searchByTitle() {
    this.newVideoList = this.searchPipe.transform(this.videoList, this.currentValue);
    this.newListEvent.emit(this.newVideoList)

  }
  clear(){
    this.currentValue='';
    this.newListEvent.emit(this.videoList);

  }
}
