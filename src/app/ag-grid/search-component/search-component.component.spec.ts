import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SearchPipe } from 'src/app/services/search.pipe';

import { SearchComponentComponent } from './search-component.component';

describe('SearchComponentComponent', () => {
  let component: SearchComponentComponent;
  let fixture: ComponentFixture<SearchComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('pipe result test', () => {
    const pipe = new SearchPipe();
    component.videoList =[]
    const ret = pipe.transform(component.videoList,'d');
    expect(ret.length).toBeLessThanOrEqual(component.videoList.length);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should correctly value the passed @Input value', () => {
    component.videoList = []; 
    fixture.detectChanges(); 
    const compiled = fixture.debugElement.nativeElement;
    expect(component.newVideoList).toBe(component.videoList); 
  });

  it('should correctly @Output ', () => {
    spyOn(component.newListEvent, 'emit'); 
    const button = fixture.nativeElement.querySelector('button');
    fixture.nativeElement.querySelector('input').value = component.videoList;
    const inputText = fixture.nativeElement.querySelector('input').value;
    button.click();
    fixture.detectChanges();
    expect(component.newListEvent.emit).toHaveBeenCalledWith(inputText); 
  });
});
